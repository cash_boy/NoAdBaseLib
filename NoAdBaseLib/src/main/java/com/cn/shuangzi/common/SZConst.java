package com.cn.shuangzi.common;

/**
 * Created by CN.
 */

public class SZConst {
    public static final int TIME_OUT = 12000;
    public static final String API_SERVICE_ERROR_EVENT_ID = "api_service_error";
    public static final String PLATFORM_LOCAL = "local";
    public static final String ERROR_ID = "errorId";
    public static final String ERROR_MSG = "errorMsg";
    public static final String ERROR_CODE = "errorCode";
    public static final String SZ_APPID = "SZ_APPID";
    public static final String ENDPOINT = "ENDPOINT";
    public static final String BUCKET = "BUCKET";

    public static final String ALIPAY = "ALIPAY";
    public static final String WXPAY = "WXPAY_APP";
    public static final String GOLD_MEMBER = "GOLD_MEMBER";

    public static final int LIMIT = 30;
    public static final String USER_INFO_SZ = "sz_user_info";
    public static final String SETTING = "setting";
    public static final String FIRST_LOAD_1 = "first_load_1";


    public static final String YES = "1";
    public static final String NO = "0";
    public static final String VIP_INFO = "vip_info";

}
