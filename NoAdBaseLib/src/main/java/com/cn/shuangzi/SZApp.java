package com.cn.shuangzi;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;

import com.cn.shuangzi.activity.SZSplashActivity;
import com.cn.shuangzi.bean.VipInfo;
import com.cn.shuangzi.common.AppIdEmptyException;
import com.cn.shuangzi.common.SZConst;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.Stack;


/**
 * Created by CN on 2016/10/31.
 */
public abstract class SZApp extends MultiDexApplication {
    protected static SZApp INSTANCE;
    private Stack<Activity> stackActivity;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        syncIsDebug(this);
        SZManager.getInstance().init(this, isDebug(),isNeedInitUMConfig());
    }
    public boolean isNeedInitUMConfig(){
        return true;
    }
    public static SZApp getInstance() {
        return INSTANCE;
    }

    private Boolean isDebug = null;

    public boolean isDebug() {
        return isDebug == null ? false : isDebug.booleanValue();
    }

    private void syncIsDebug(Context context) {
        if (isDebug == null) {
            isDebug = context.getApplicationInfo() != null &&
                    (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        }
    }

    public Stack<Activity> getStackActivity() {
        if (stackActivity == null) {
            stackActivity = new Stack<>();
        }
        return stackActivity;
    }

    public Activity getTopActivity() {
        if (stackActivity != null && stackActivity.size() > 0) {
            return stackActivity.get(stackActivity.size() - 1);
        }
        return null;
    }

    public void addActivity(Activity activity) {
        if (stackActivity == null)
            stackActivity = new Stack<>();
        if (activity != null)
            stackActivity.add(activity);
    }

    public void removeActivity(Activity activity) {
        if (stackActivity != null && activity != null) {
            stackActivity.remove(activity);
        }
    }

    public void finishSplashActivity() {
        if (stackActivity != null) {
            for (Activity activity : stackActivity) {
                if (activity instanceof SZSplashActivity) {
                    activity.finish();
                    return;
                }
            }
        }
    }

    public void removeAndFinishAllActivity() {
        if (stackActivity != null) {
            for (Activity activity : stackActivity) {
                activity.finish();
            }
            stackActivity.clear();
        }
    }

    public int getActivityCount() {
        if (stackActivity != null) {
            return stackActivity.size();
        }
        return 0;
    }

    public <T extends Activity> void removeActivities(Class<T>... classActivities){
        for (Activity activity: getStackActivity()){
            for (Class classActivity : classActivities){
                if(activity.getClass().getName().equals(classActivity.getName())){
                    activity.finish();
                }
            }
        }
    }

    public <T extends Activity> void removeActivitiesExcept(Class<T>... classActivities){
        for (Activity activity: getStackActivity()){
            for (Class classActivity : classActivities){
                if(!activity.getClass().getName().equals(classActivity.getName())){
                    activity.finish();
                }
            }
        }
    }

    public String getSZAppId() {
        String appId = SZUtil.getSZ_APPID(this);
        if (TextUtils.isEmpty(appId)) {
            throw new AppIdEmptyException();
        }
        return appId;
    }

    public abstract String getUserToken();


}
