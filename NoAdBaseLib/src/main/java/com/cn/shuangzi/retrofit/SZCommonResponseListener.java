package com.cn.shuangzi.retrofit;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZManager;
import com.cn.shuangzi.common.SZCommonEvent;
import com.cn.shuangzi.common.SZConst;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by CN.
 */

public abstract class SZCommonResponseListener implements SZRetrofitResponseListener {
    private boolean isToast;

    public SZCommonResponseListener(boolean isToast) {
        this.isToast = isToast;
    }

    public SZCommonResponseListener() {
        isToast = true;
    }

    @Override
    public void onSuccess(String data) {
        onResponseSuccess(data);
    }

    @Override
    public void onNetError(int errorCode, String errorMsg) {
        SZUtil.log("errorCode:" + errorCode + "|errorMsg:" + errorMsg);
        onWebServiceError(errorCode,null, SZManager.getInstance().getContext().getString(R.string.error_net_work));
    }

    @Override
    public void onWebServiceError(int errorCode,String errorId, String errorMsg) {
        SZUtil.log("errorCode:" + errorCode + "|errorMsg:" + errorMsg);
        SZManager.getInstance().onUMEvent(SZConst.API_SERVICE_ERROR_EVENT_ID, SZUtil.getErrorEventMap(SZConst.PLATFORM_LOCAL,errorId, String.valueOf(errorCode), errorMsg));
        if (isToast) {
            if(ERROR_WECHAT_CODE_USED==errorCode) {
                if(isShowWechatUsedError) {
                    SZToast.error(errorMsg);
                }
            }else{
                SZToast.error(errorMsg);
            }
        }
        onResponseError(errorCode, errorMsg);
        if(errorCode == 4002022 || errorCode == 4002023 || errorCode == 4002045){
            EventBus.getDefault().post(new SZCommonEvent(SZCommonEvent.LOGOUT_EVENT,errorMsg));
        }
    }

    public abstract void onResponseSuccess(String data);

    public abstract void onResponseError(int errorCode, String errorMsg);

}
