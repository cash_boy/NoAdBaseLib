package com.cn.shuangzi.retrofit;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by CN.
 */

public interface SZRequest {

    @POST("/ApplicationService/InspectApplicationVersion")
    @FormUrlEncoded
    Observable<String> getNewVersionInfo(@Field("applicationId") String applicationId, @Field("platform") String platform, @Field("version") int version);

    @POST("/UserService/ConsumeCode")
    @FormUrlEncoded
    Observable<String> loginThird(@Field("applicationId") String applicationId, @Field("platformType") String platformType, @Field("authCode") String authCode);

    @POST("/UserService/SendCaptcha")
    @FormUrlEncoded
    Observable<String> getVerificationCode(@Field("applicationId") String applicationId, @Field("phone") String phone);

    @POST("/UserService/ConsumeCaptcha")
    @FormUrlEncoded
    Observable<String> getTokenByVerificationCode(@Field("applicationId") String applicationId, @Field("phone") String phone, @Field("captcha") String captcha);

    @POST("/UserService/UpdateConsumerInfo")
    @FormUrlEncoded
    Observable<String> updateUserInfo(@Field("consumerId") String consumerId, @Field("userName") String userName, @Field("avatarUrl") String avatarUrl);

    @POST("/OpinionsService/GiveOpinions?platform=ANDROID")
    @FormUrlEncoded
    Observable<String> submitFeedback(@Field("applicationId") String applicationId,@Field("consumerId") String consumerId, @Field("opinions") String content
           ,@Field("version") String version);

    @POST("/UserService/GetUserInfo")
    @FormUrlEncoded
    Observable<String> getUserInfo(@Field("ticketId") String ticketId);

    @POST("/UserService/GetSTSSign")
    Observable<String> getSTSSign();


    @POST("/PayService/QueryFixedPrice")
    @FormUrlEncoded
    Observable<String> getVipPrice(@Field("applicationId") String applicationId);

    @POST("/PayService/SubmitOrder")
    @FormUrlEncoded
    Observable<String> submitOrder(@Field("consumerId") String consumerId,@Field("payType") String payType,@Field("deviceId") String deviceId,@Field("fixedPriceId") String fixedPriceId);

}
