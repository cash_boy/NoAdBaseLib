package com.cn.shuangzi.retrofit;

import com.cn.shuangzi.SZManager;
import com.cn.shuangzi.common.SZConst;
import com.cn.shuangzi.util.SZUtil;

/**
 * Created by CN on 2018-3-13.
 */

public interface SZRetrofitResponseListener {
    public static final int ERROR_WECHAT_CODE_USED = 40018;
    public static boolean isShowWechatUsedError = false;
    void onSuccess(String data);

    void onNetError(int errorCode, String errorMsg);

    void onWebServiceError(int errorCode,String errorId, String errorMsg);

    abstract class SimpleSZRetrofitResponseListener implements SZRetrofitResponseListener {
        public void onWebServiceError(int errorCode,String errorId, String errorMsg) {
            SZManager.getInstance().onUMEvent(SZConst.API_SERVICE_ERROR_EVENT_ID, SZUtil.getErrorEventMap(SZConst.PLATFORM_LOCAL, errorId,String.valueOf(errorCode), errorMsg));

            onNetError(errorCode, errorMsg);
        }
    }
}
