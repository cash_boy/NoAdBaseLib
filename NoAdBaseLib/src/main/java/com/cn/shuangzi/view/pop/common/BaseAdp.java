package com.cn.shuangzi.view.pop.common;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;


public abstract class BaseAdp<T extends Object> extends BaseAdapter {
    protected List<T> list;
    protected Context context;
    private int layoutId;
    public BaseAdp(Context context, List<T> list, int layoutId) {
    	this.context = context;
    	this.list = list;
    	this.layoutId = layoutId;
	}
    @Override
    public int getCount() {
        return list==null?0:list.size();
    }
    @Override
    public Object getItem(int arg0) {
        return list==null?null:list.get(arg0);
    }
    @Override
    public long getItemId(int arg0) {
        return arg0;
    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
    	ViewHolder holder = ViewHolder.getViewHolder(context, view, layoutId);
    	onGetView(position,holder);
    	view = holder.getMConvertView();
    	if(view == null)
    		throw new RuntimeException("the returned value of onGetView is null");
    	view.setTag(holder);
        return view;
    }
    public abstract void onGetView(int position, ViewHolder holder);

    public void notifyDataSetChanged(int position,View childView){
//        View mView = lv.getChildAt(itemIndex-lv.getFirstVisiblePosition());//获取指定itemIndex在屏幕中的view
        onGetView(position,ViewHolder.getViewHolder(context, childView, layoutId));
    }
}
