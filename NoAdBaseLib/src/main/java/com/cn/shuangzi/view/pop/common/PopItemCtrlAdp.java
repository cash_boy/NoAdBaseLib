package com.cn.shuangzi.view.pop.common;

import android.content.Context;
import android.widget.TextView;

import com.cn.shuangzi.R;
import com.cn.shuangzi.util.SZTextViewWriterUtil;

import java.util.List;

/**
 * Created by CN on 2017-5-22.
 */

public class PopItemCtrlAdp extends BaseAdp {
    private List<CtrlItem> ctrlItemList;

    public PopItemCtrlAdp(Context context, List<CtrlItem> ctrlItemList) {
        super(context, ctrlItemList, R.layout.adp_pop_ctrl);
        this.ctrlItemList = ctrlItemList;
    }

    @Override
    public void onGetView(int position, ViewHolder holder) {
        CtrlItem ctrlItem = ctrlItemList.get(position);
        if (ctrlItem != null) {
            SZTextViewWriterUtil.writeValue((TextView) holder.getView(R.id.txtDesc), ctrlItem.getText());
            holder.setGone(R.id.imgChecked, ctrlItem.isChecked());
        }
    }
}
