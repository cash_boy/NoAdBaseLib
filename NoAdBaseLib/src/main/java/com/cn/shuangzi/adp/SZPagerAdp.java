package com.cn.shuangzi.adp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.cn.shuangzi.SZBaseFragment;
import com.cn.shuangzi.util.SZValidatorUtil;

import java.util.List;


public class SZPagerAdp extends FragmentPagerAdapter {
    private List<? extends SZBaseFragment> fragmentList;
    public SZPagerAdp(FragmentManager fm, List<? extends SZBaseFragment> fragmentList) {
        super(fm);
        this.fragmentList = fragmentList;
    }

    @Override
    public int getCount() {
        if(SZValidatorUtil.isValidList(fragmentList)) {
            return fragmentList.size();
        }else {
            return 0;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentList != null && position < fragmentList.size() ? fragmentList.get(position).getTitle() == null ? "" : fragmentList.get(position).getTitle() : "";
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }
}
