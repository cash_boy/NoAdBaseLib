package com.cn.shuangzi.adp;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.cn.shuangzi.R;
import com.cn.shuangzi.util.SZImageLoader;

import java.util.List;

/**
 * Created by CN.
 */

public abstract class SZBaseAdp<T extends Object> extends BaseQuickAdapter<T,BaseViewHolder> {
    private Context context;
    private SZImageLoader imageLoader;
    public SZBaseAdp(int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
        setEmptyView(false,0,0);
    }
    public SZBaseAdp(Context context, int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
        this.context = context;
        if(context!=null){
            imageLoader = new SZImageLoader(context);
        }
        setEmptyView(true,0,R.mipmap.ic_empty);
    }
    public SZBaseAdp(int layoutResId, @Nullable List<T> data, View emptyView) {
        super(layoutResId, data);
        setEmptyView(emptyView);
    }
    public SZBaseAdp(Context context,int layoutResId, @Nullable List<T> data, View emptyView) {
        super(layoutResId, data);
        if(context!=null){
            imageLoader = new SZImageLoader(context);
        }
        setEmptyView(emptyView);
    }
    public SZBaseAdp(Context context, int layoutResId, @Nullable List<T> data, int txtResEmpty) {
        super(layoutResId, data);
        this.context = context;
        if(context!=null){
            imageLoader = new SZImageLoader(context);
        }
        setEmptyView(true,txtResEmpty,R.mipmap.ic_empty);
    }
    public SZBaseAdp(Context context, int layoutResId, @Nullable List<T> data, int txtResEmpty, int imgResEmpty) {
        super(layoutResId, data);
        this.context = context;
        if(context!=null){
            imageLoader = new SZImageLoader(context);
        }
        setEmptyView(true,txtResEmpty,imgResEmpty);
    }
    public SZBaseAdp(Context context, int layoutResId, @Nullable List<T> data, boolean hasEmpty, int txtResEmpty, int imgResEmpty) {
        super(layoutResId, data);
        this.context = context;
        if(context!=null){
            imageLoader = new SZImageLoader(context);
        }
        setEmptyView(hasEmpty,txtResEmpty,imgResEmpty);
    }
    private void setEmptyView(boolean hasEmpty, int txtResEmpty, int imgResEmpty){
        if(hasEmpty) {
            View emptyView = LayoutInflater.from(context).inflate(R.layout.item_empty, null);
            if(txtResEmpty!=0) {
                TextView txtEmpty = emptyView.findViewById(R.id.txtEmpty);
                txtEmpty.setText(txtResEmpty);
            }
            if(imgResEmpty!=0) {
                ImageView imgEmpty = emptyView.findViewById(R.id.imgEmpty);
                imgEmpty.setBackgroundResource(imgResEmpty);
            }
            setEmptyView(emptyView);
        }
    }
    public SZImageLoader getImageLoader() {
        return imageLoader;
    }

    public Context getContext() {
        return context;
    }
}
