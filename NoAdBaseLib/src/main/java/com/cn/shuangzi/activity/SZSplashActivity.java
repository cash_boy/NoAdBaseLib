package com.cn.shuangzi.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cn.shuangzi.R;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.util.SZXmlUtil;
import com.cn.shuangzi.view.AlertWidget;


/**
 * Created by CN on 2017-11-28.
 */

public abstract class SZSplashActivity extends SZBaseActivity {
    protected FrameLayout splash_container;
    protected TextView txtSkip;
    protected TextView txtVipSkip;
    protected View rltSkip;
    protected RelativeLayout rltSplashBg;
    protected RelativeLayout rltBottom;
    protected ImageView imgApp;
    protected TextView txtAppName;
    protected Handler handlerCtrl;
    protected boolean isToMain;


    public static void startOnce(Activity from, Class to) {
        Intent intent = new Intent(from, to);
        Bundle data = new Bundle();
        data.putBoolean("isToMain", false);
        intent.putExtras(data);
        from.startActivity(intent);
        from.overridePendingTransition(0, 0);
    }
    @Override
    public void preLoadChildView(){
        final View decorView = getWindow().getDecorView();
        final int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int i) {
                if ((i & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    decorView.setSystemUiVisibility(uiOptions);
                }
            }
        });
    }
    @Override
    protected int onGetChildView() {
        return R.layout.activity_sz_splash;
    }

    @Override
    protected void onBindChildViews() {
        rltSplashBg = findViewById(R.id.rltSplashBg);
        splash_container = findViewById(R.id.splash_container);
        rltBottom = findViewById(R.id.rltBottom);
        imgApp = findViewById(R.id.imgApp);
        txtAppName = findViewById(R.id.txtAppName);
        txtSkip = findViewById(R.id.txtSkip);
        txtVipSkip = findViewById(R.id.txtVipSkip);
        rltSkip = findViewById(R.id.rltSkip);

    }

    @Override
    protected void onBindChildListeners() {
    }

    @Override
    protected void onChildViewCreated() {
        // 解决初次安装后打开后按home返回后重新打开重启问题。。。
        if (!this.isTaskRoot()) { //判断该Activity是不是任务空间的源Activity，“非”也就是说是被系统重新实例化出来
            //如果你就放在launcher Activity中话，这里可以直接return了
            Intent mainIntent = getIntent();
            String action = mainIntent.getAction();
            if (mainIntent.hasCategory(Intent.CATEGORY_LAUNCHER) && action.equals(Intent.ACTION_MAIN)) {
                finish();
                return;//finish()之后该活动会继续执行后面的代码，你可以logCat验证，加return避免可能的exception
            }
        }
        isToMain = getIntent().getBooleanExtra("isToMain", true);
        handlerCtrl = new Handler();
        initSplashInfo();
        hideNavigationBar();
        onPreCreated();
        onCreated();
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }

    public void initSplashInfo() {
        if (getSplashRes() != 0) {
            rltSplashBg.setBackgroundResource(getSplashRes());
        }
        if (getAppIconRes() != 0) {
            imgApp.setImageResource(getAppIconRes());
        }
        if (getAppNameRes() != 0) {
            txtAppName.setText(getAppNameRes());
        }
        if (getAppNameColorRes() != 0) {
            txtAppName.setTextColor(getResources().getColor(getAppNameColorRes()));
        }
    }


    private boolean canJump;

    public void next() {
        if (canJump) {
            toMain(0);
        } else {
            canJump = true;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        canJump = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (canJump) {
            next();
        }
        canJump = true;
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    public void toMain(long delay) {
        if(handlerCtrl!=null) {
            handlerCtrl.removeCallbacksAndMessages(null);
            if (delay == 0) {
                toMain();
                return;
            }
            handlerCtrl.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toMain();
                }
            }, delay);
        }
    }

    public void toMain() {
        if(handlerCtrl!=null) {
            if (!isToMain) {
                finish();
                overridePendingTransition(0, 0);
            } else {
                Intent intent = new Intent(this, getHomeActivity());
                startActivity(intent);
                finish();
            }
        }
    }

    public void showServiceAlert() {
        showServiceAlert(null, null, null, null);
    }

    public void showServiceAlert(String title, String desc, String txtUserServiceAgreement, String txtPrivacyPolicy) {
        final SZXmlUtil xmlUtil = new SZXmlUtil(this, "privacy_alert");
        if (!xmlUtil.getBoolean("is_privacy_alert")) {
            final AlertWidget alertWidget = new AlertWidget(this);
            alertWidget.show(R.layout.item_privacy_alert);
            alertWidget.setCancelable(false);
            TextView txtPrivacyTitle = alertWidget.getWindow().findViewById(R.id.txtPrivacyTitle);
            TextView txtPrivacyDesc = alertWidget.getWindow().findViewById(R.id.txtPrivacyDesc);
            TextView txtClose = alertWidget.getWindow().findViewById(R.id.txtClose);
            TextView txtAgree = alertWidget.getWindow().findViewById(R.id.txtAgree);
            txtClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertWidget.close();
                    finish();
                }
            });
            txtAgree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    xmlUtil.put("is_privacy_alert", true);
                    alertWidget.close();
                    onClickAgreeUse();
                }
            });
            if (title != null) {
                txtPrivacyTitle.setText(title);
            }
            if (txtUserServiceAgreement == null) {
                txtUserServiceAgreement = getString(R.string.service_agreement_symbol);
            }
            if (txtPrivacyPolicy == null) {
                txtPrivacyPolicy = getString(R.string.privacy_policy_symbol);
            }
            if (desc == null) {
                desc = getString(R.string.privacy_content, getString(getAppNameRes()),txtUserServiceAgreement,txtPrivacyPolicy);
            }
            SpannableStringBuilder stringBuilder = new SpannableStringBuilder(desc);
            AgreementTextViewSpan agreementTextViewSpan = new AgreementTextViewSpan();
            stringBuilder.setSpan(agreementTextViewSpan, desc.indexOf(txtUserServiceAgreement), desc.indexOf(txtUserServiceAgreement) + txtUserServiceAgreement.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            PolicyTextViewSpan policyTextViewSpan = new PolicyTextViewSpan();
            stringBuilder.setSpan(policyTextViewSpan, desc.indexOf(txtPrivacyPolicy), desc.indexOf(txtPrivacyPolicy) + txtPrivacyPolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            txtPrivacyDesc.setText(stringBuilder);
            txtPrivacyDesc.setHighlightColor(getResources().getColor(android.R.color.transparent));
            txtPrivacyDesc.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            onClickAgreeUse();
        }

    }

    private class AgreementTextViewSpan extends ClickableSpan {
        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(getResources().getColor(R.color.colorPrimary));
            ds.setUnderlineText(false);
        }

        @Override
        public void onClick(View widget) {
            onClickUserServiceAgreement();
        }
    }

    private class PolicyTextViewSpan extends ClickableSpan {
        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(getResources().getColor(R.color.colorPrimary));
            ds.setUnderlineText(false);
        }

        @Override
        public void onClick(View widget) {
            onClickPrivacyPolicy();
        }
    }

    @Override
    public boolean isShowTitleInit() {
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handlerCtrl != null) {
            handlerCtrl.removeCallbacksAndMessages(null);
        }
        handlerCtrl = null;
    }

    public abstract void onPreCreated();

    public abstract void onCreated();

    public abstract int getSplashRes();

    public abstract int getAppIconRes();

    public abstract int getAppNameRes();

    public abstract int getAppNameColorRes();

    public abstract Class getHomeActivity();

    public abstract void onClickUserServiceAgreement();

    public abstract void onClickPrivacyPolicy();

    public abstract void onClickAgreeUse();
}
