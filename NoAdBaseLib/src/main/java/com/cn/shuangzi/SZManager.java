package com.cn.shuangzi;

import android.content.Context;

import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;

import java.util.Map;

/**
 * Created by CN.
 */

public class SZManager {
    private static SZManager INSTANCE;
    private boolean isDebugMode;
    private Context mContext;
    private String appKeyUM;
    private String appChannel;
    private String appMessageSecret;
    private boolean isNeedInitUMConfig = true;
    private SZManager() {
    }

    public static SZManager getInstance() {
        if (INSTANCE == null) {
            synchronized (SZManager.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SZManager();
                }
            }
        }
        return INSTANCE;
    }

    public void init(Context mContext) {
        this.init(mContext, false);
    }

    public void init(Context mContext, boolean isDebugMode) {
        this.init(mContext,isDebugMode,true);
    }
    public void init(Context mContext, boolean isDebugMode,boolean isNeedInitUMConfig) {
        this.mContext = mContext;
        this.isDebugMode = isDebugMode;
        SZToast.init(mContext);
        this.isNeedInitUMConfig = isNeedInitUMConfig;
        appKeyUM = SZUtil.getAppMetaDataString(mContext,"UMENG_APPKEY");
        appChannel = SZUtil.getAppMetaDataString(mContext,"UMENG_CHANNEL");
        appMessageSecret = SZUtil.getAppMetaDataString(mContext,"UMENG_MESSAGE_SECRET");
        if(isNeedInitUMConfig) {
            initUMConfig(mContext);
        }
    }

    public Context getContext() {
        return mContext;
    }
    public void cancelRequest(String tag) {
        if (tag != null) {
            SZRetrofitManager.getInstance().cancelDisposable(tag);
        } else {
            SZRetrofitManager.getInstance().cancelAllDisposable();
        }
    }

    public boolean isDebugMode() {
        return isDebugMode;
    }

    /**********************************************
     * 统计信息
     ***********************************************/
    public void initUMConfig(Context mContext) {
        UMConfigure.init(mContext, appKeyUM,appChannel, UMConfigure.DEVICE_TYPE_PHONE, appMessageSecret);
        MobclickAgent.setScenarioType(mContext, MobclickAgent.EScenarioType.E_UM_NORMAL);
        MobclickAgent.setCatchUncaughtExceptions(true);
        MobclickAgent.openActivityDurationTrack(false);
    }

    public String getAppChannel() {
        return appChannel;
    }

    public void onUMResume(Context context) {
        if (isUMInit()) {
            MobclickAgent.onResume(context);
        }
    }

    public void onUMPause(Context context) {
        if (isUMInit()) {
            MobclickAgent.onPause(context);
        }
    }

    public void onUMPageStart(String viewName) {
        if (isUMInit()) {
            MobclickAgent.onPageStart(viewName);
        }
    }

    public void onUMPageEnd(String viewName) {
        if (isUMInit()) {
            MobclickAgent.onPageEnd(viewName);
        }
    }

    public void onUMEvent(String eventID) {
        if (isUMInit()) {
            MobclickAgent.onEvent(mContext, eventID);
        }
    }

    public void onUMEvent(String eventID, String label) {
        if (isUMInit()) {
            MobclickAgent.onEvent(mContext, eventID, label);
        }
    }

    public void onUMEvent(String eventID, Map<String, String> map) {
        if (isUMInit()) {
            MobclickAgent.onEvent(mContext, eventID, map);
        }
    }

    public void onUMEventValue(String eventID, Map<String, String> map, int du) {
        if (isUMInit()) {
            MobclickAgent.onEventValue(mContext, eventID, map, du);
        }
    }

    private boolean isUMInit() {
        if (appKeyUM == null && isNeedInitUMConfig) {
            throw new IllegalStateException("you doesn't init UMConfig!");
        }
        return true;
    }

}
