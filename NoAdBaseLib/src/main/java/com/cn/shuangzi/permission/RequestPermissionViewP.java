package com.cn.shuangzi.permission;

/**
 * Created by CN on 2017-10-11.
 */

public interface RequestPermissionViewP{
    void onPermissionRequestSuccess(String[] permissionName);
    void onRequestPermissionAlertCancelled(String permissionsNeed);
}
