package com.cn.shuangzi.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */

public class VersionInfo implements Serializable{
    private boolean lastVersion;
    private boolean isRequired;
    private String downloadUrl;
    private String versionText;
    private String describe;


    public boolean isRequired() {
        return isRequired;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public String getVersionText() {
        return versionText;
    }

    public String getDescribe() {
        return describe;
    }

    public boolean isLastVersion() {
        return lastVersion;
    }

    public void setLastVersion(boolean lastVersion) {
        this.lastVersion = lastVersion;
    }

    public void setRequired(boolean required) {
        isRequired = required;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public void setVersionText(String versionText) {
        this.versionText = versionText;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    @Override
    public String toString() {
        return "VersionInfo{" +
                "lastVersion=" + lastVersion +
                ", isRequired=" + isRequired +
                ", downloadUrl='" + downloadUrl + '\'' +
                ", versionText='" + versionText + '\'' +
                ", describe='" + describe + '\'' +
                '}';
    }
}
