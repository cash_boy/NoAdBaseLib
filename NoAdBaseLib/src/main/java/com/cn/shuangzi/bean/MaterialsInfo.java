package com.cn.shuangzi.bean;

import android.content.Context;

import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by CN.
 */

public class MaterialsInfo implements Serializable{

    public static final String ACTION_PAGE = "PAGE";
    public static final String ACTION_WEB = "WEB_VIEW";
    public static final String ACTION_APP_STORE = "APP_STORE";

    private String action;
    private String extended;
    private String target;
    private String imageURL;

    public String getExtended() {
        return extended;
    }

    public String getImage() {
        return imageURL;
    }

    public String getAction() {
        return action;
    }

    public <T extends Object> T getExtended(Class<T> classT) {
        return new Gson().fromJson(extended,classT);
    }
    public String getTarget() {
        return target;
    }

    public String getPackageNameInAppStore(){
        AppStoreInfo appStoreInfo = getExtended(AppStoreInfo.class);
        return appStoreInfo.getPackageName();
    }
    public boolean launchApp(Context context){
        String packageName = getPackageNameInAppStore();
        if(SZValidatorUtil.isValidString(packageName)){
            if(SZUtil.isAppInstalled(packageName)){
                SZUtil.launchAPP(context,packageName);
            }else{
                SZUtil.launchAppStoreDetail(context,packageName,null);
            }
            return true;
        }
        return false;
    }
}
