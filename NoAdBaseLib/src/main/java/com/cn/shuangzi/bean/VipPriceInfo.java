package com.cn.shuangzi.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */

public class VipPriceInfo implements Serializable{
    private String fpApplicationId;
    private String fpId;
    private String fpPrice;
    private String fpName;
    private String fpSubject;
    private String fpTag;
    private String fpNo;
    private String fpOriginalPrice;
    private String fpMode;
    private boolean fpIsDeleted;
    private boolean fpIsNotice;
    private long fpCreateTime;
    private long fpUpdateTime;
    private boolean isCheck;

    public void setFpApplicationId(String fpApplicationId) {
        this.fpApplicationId = fpApplicationId;
    }

    public void setFpId(String fpId) {
        this.fpId = fpId;
    }

    public void setFpPrice(String fpPrice) {
        this.fpPrice = fpPrice;
    }

    public void setFpName(String fpName) {
        this.fpName = fpName;
    }

    public void setFpSubject(String fpSubject) {
        this.fpSubject = fpSubject;
    }

    public void setFpTag(String fpTag) {
        this.fpTag = fpTag;
    }

    public void setFpOriginalPrice(String fpOriginalPrice) {
        this.fpOriginalPrice = fpOriginalPrice;
    }

    public void setFpMode(String fpMode) {
        this.fpMode = fpMode;
    }

    public boolean isSpecialPrice(){
        return "discount".equalsIgnoreCase(fpMode);
    }
    public String getFpApplicationId() {
        return fpApplicationId;
    }

    public String getFpId() {
        return fpId;
    }

    public String getFpPrice() {
        return fpPrice;
    }

    public String getFpName() {
        return fpName;
    }

    public String getFpSubject() {
        return fpSubject;
    }

    public String getFpTag() {
        return fpTag;
    }

    public String getFpNo() {
        return fpNo;
    }

    public String getFpOriginalPrice() {
        return fpOriginalPrice;
    }

    public boolean isFpIsDeleted() {
        return fpIsDeleted;
    }

    public boolean isFpIsNotice() {
        return fpIsNotice;
    }

    public long getFpCreateTime() {
        return fpCreateTime;
    }

    public long getFpUpdateTime() {
        return fpUpdateTime;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    @Override
    public String toString() {
        return "VipPriceInfo{" +
                "fpApplicationId='" + fpApplicationId + '\'' +
                ", fpId='" + fpId + '\'' +
                ", fpPrice='" + fpPrice + '\'' +
                ", fpName='" + fpName + '\'' +
                ", fpSubject='" + fpSubject + '\'' +
                ", fpTag='" + fpTag + '\'' +
                ", fpNo='" + fpNo + '\'' +
                ", fpOriginalPrice='" + fpOriginalPrice + '\'' +
                ", fpIsDeleted=" + fpIsDeleted +
                ", fpIsNotice=" + fpIsNotice +
                ", fpCreateTime=" + fpCreateTime +
                ", fpUpdateTime=" + fpUpdateTime +
                '}';
    }
}
