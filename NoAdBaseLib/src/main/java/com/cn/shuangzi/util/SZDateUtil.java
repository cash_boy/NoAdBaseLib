package com.cn.shuangzi.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by CN on 2016/9/29.
 */

public class SZDateUtil {
    public static final long ONE_DAY = 24 * 60 * 60 * 1000;
    public static final long ONE_HOUR = 60 * 60 * 1000;
    public static final long ONE_MINUTE = 60 * 1000;

    private static SimpleDateFormat sfAllShow = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
    private static SimpleDateFormat sfYearMonthDateShow = new SimpleDateFormat("yyyy年MM月dd日");
    private static SimpleDateFormat sfYearMonthShow = new SimpleDateFormat("yyyy年MM月");
    private static SimpleDateFormat sfAllTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat sfSimpleYearMonthDate = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat sfMonthDateHourMinuteShow = new SimpleDateFormat("MM月dd日 HH:mm");
    public static SimpleDateFormat sfMonthDateShow = new SimpleDateFormat("MM月dd日");

    public static String getMonthDayDate(Date date) {
        return sfMonthDateShow.format(date);
    }
    public static String getMonthDayHourMinuteDate(Date date) {
        return sfMonthDateHourMinuteShow.format(date);
    }
    public static String getDataDateAll(Date date) {
        return sfAllTime.format(date);
    }

    public static int getYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR) - 1900;
    }

    public static int getMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.MONTH);
    }

    public static String getShowMonth(Date date) {
        return String.valueOf(getMonth(date) + 1);
    }

    public static String getShowDateAll(Date date) {
        return sfAllShow.format(date);
    }

    public static String getShowYearMonthDate(Date date) {
        try {
            return sfYearMonthDateShow.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getShowYearMonth(Date date) {
        try {
            return sfYearMonthShow.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getWeekShow(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String week = "";
        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                week = "周一";
                break;
            case Calendar.TUESDAY:
                week = "周二";
                break;
            case Calendar.WEDNESDAY:
                week = "周三";
                break;
            case Calendar.THURSDAY:
                week = "周四";
                break;
            case Calendar.FRIDAY:
                week = "周五";
                break;
            case Calendar.SATURDAY:
                week = "周六";
                break;
            case Calendar.SUNDAY:
                week = "周日";
                break;
        }
        return week;
    }

    public static Date getOneDateBegin(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    public static Date getOneDateBegin(int year, int month, int date) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year + 1900, month, date, 0, 0, 0);
        return calendar.getTime();
    }

    public static Date getOneDateEnd(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }

    public static Date getOneDateEnd(int year, int month, int date) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year + 1900, month, date, 23, 59, 59);
        return calendar.getTime();
    }

    public static int getDays(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static boolean isSameDay(Date date1, Date Date2) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date1);
        int year1 = calendar.get(Calendar.YEAR);
        int day1 = calendar.get(Calendar.DAY_OF_YEAR);

        calendar.setTime(Date2);
        int year2 = calendar.get(Calendar.YEAR);
        int day2 = calendar.get(Calendar.DAY_OF_YEAR);

        if ((year1 == year2) && (day1 == day2)) {
            return true;
        }
        return false;
    }

    /**
     * 日期比较，date1前于date2
     *
     * @param date1
     * @param Date2
     * @return
     */
    public static boolean compareDay(Date date1, Date Date2) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date1);
        int year1 = calendar.get(Calendar.YEAR);
        int day1 = calendar.get(Calendar.DAY_OF_YEAR);

        calendar.setTime(Date2);
        int year2 = calendar.get(Calendar.YEAR);
        int day2 = calendar.get(Calendar.DAY_OF_YEAR);

        if ((year1 < year2) || (((year1 == year2)) && (day1 < day2))) {
            return true;
        }
        return false;
    }

    public static boolean isSameMonth(Date date1, Date Date2) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date1);
        int year1 = calendar.get(Calendar.YEAR);
        int month1 = calendar.get(Calendar.MONTH);

        calendar.setTime(Date2);
        int year2 = calendar.get(Calendar.YEAR);
        int month2 = calendar.get(Calendar.MONTH);

        if ((year1 == year2) && (month1 == month2)) {
            return true;
        }
        return false;
    }

    public static Date getAfterDate(Date date, int afterDayCount) {
        return new Date((date.getTime() + (ONE_DAY * afterDayCount)));
    }

    public static int getDayDistance(Date dateBefore, Date dateAfter) {
        Calendar calendarBefore = Calendar.getInstance();
        calendarBefore.setTime(dateBefore);
        Calendar calendarAfter = Calendar.getInstance();
        calendarAfter.setTime(dateAfter);
        return (int) ((calendarAfter.getTimeInMillis() - calendarBefore.getTimeInMillis()) / ONE_DAY);
    }

    public static String getSimpleYearMonthDateTime(Date date) {
        try {
            return sfSimpleYearMonthDate.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static Date getSimpleYearMonthDateTime(String date) {
        try {
            return sfSimpleYearMonthDate.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 判断是否为昨天(效率比较高)
     */
    public static boolean isYesterday(Date date) {

        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        if (cal.get(Calendar.YEAR) == (pre.get(Calendar.YEAR))) {
            int diffDay = cal.get(Calendar.DAY_OF_YEAR)
                    - pre.get(Calendar.DAY_OF_YEAR);

            if (diffDay == -1) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否为今天(效率比较高)
     */
    public static boolean isToday(Date date) {

        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if (cal.get(Calendar.YEAR) == (pre.get(Calendar.YEAR))) {
            int diffDay = cal.get(Calendar.DAY_OF_YEAR)
                    - pre.get(Calendar.DAY_OF_YEAR);
            if (diffDay == 0) {
                return true;
            }
        }
        return false;
    }

    public static boolean isThisYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year1 = calendar.get(Calendar.YEAR);

        calendar.setTime(new Date());
        int yearThis = calendar.get(Calendar.YEAR);
        return year1 == yearThis;
    }

    public static Date getPreMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, -1);
        return calendar.getTime();
    }

    public static Date getNextMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, 1);
        return calendar.getTime();
    }

    // 获取本周的开始时间
    public static Date getBeginDayOfWeek() {
        Calendar cal = Calendar.getInstance();
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == 1) {
            dayOfWeek += 7;
        }
        cal.add(Calendar.DATE, 2 - dayOfWeek);
        return cal.getTime();
    }

    // 获取本周的结束时间
    public static Date getEndDayOfWeek() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getBeginDayOfWeek());
        cal.add(Calendar.DAY_OF_WEEK, 6);
        Date weekEndSta = cal.getTime();
        return weekEndSta;
    }

    public static Date getMonthDayBegin() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    public static Date getMonthDayEnd() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar.getTime();
    }

    public static Date getMonthDayBegin(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    public static Date getMonthDayEnd(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar.getTime();
    }

    public static Date getYearDayBegin() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    public static Date getYearDayEnd() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DAY_OF_MONTH, 31);
        return calendar.getTime();
    }

}
