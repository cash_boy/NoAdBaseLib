package com.cn.shuangzi.util;

import android.view.View;
import android.widget.TextView;

/**
 *************************
 * TextView写值工具
 * @author CN
 *************************
 */
public class SZTextViewWriterUtil {
	public static void writeValue(TextView txtView, String value){
		if(txtView==null)
			return;
		if(value!=null){
			txtView.setText(value);
		}
	}

	public static void writeValueWithGone(TextView txtView, String value){
		if(txtView==null)
			return;
		if(SZValidatorUtil.isValidString(value)){
			txtView.setText(value);
			txtView.setVisibility(View.VISIBLE);
		}else{
			txtView.setVisibility(View.GONE);
		}
	}
	public static void writeValue(TextView txtView, String value, String defaultValue){
		if(txtView==null)
			return;
		txtView.setVisibility(View.VISIBLE);
		txtView.setText(value!=null ? value
				: defaultValue);
	}

}
