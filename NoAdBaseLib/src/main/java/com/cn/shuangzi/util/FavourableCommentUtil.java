package com.cn.shuangzi.util;

import android.content.Context;
import android.content.DialogInterface;

import com.cn.shuangzi.SZApp;
import com.cn.shuangzi.view.AlertWidget;

import java.util.Date;

/**
 * Created by CN.
 */

public class FavourableCommentUtil {
    private static final String FAVOURABLE_COMMENT_SETTING = "FavourableComment";
    private static final String USED = "USED";
    private static final String USED_DATE = "USED_DATE";
    private static final String VERSION_SHOW_TIMES = "FC_TIMES";
    private static final String CLICK_COMMENT_TIME = "CLICK_COMMENT";
    private static final int YES = 2;
    private static final int NO = 1;
    private static final String CLICK_COMMENT_STATUS = "COMMENT_STATUS";
    private static final long ONE_DAY = 24 * 60 * 60 * 1000;
    private static int PER_VERSION_MAX_SHOW_TIMES = 2;
    private static int COMMENT_INTERVAL_DAY = 7;
    private static final SZXmlUtil xmlUtil = new SZXmlUtil(SZApp.getInstance(), FAVOURABLE_COMMENT_SETTING);
    public static void init(int per_version_max_show_times,int comment_interval_day){
        PER_VERSION_MAX_SHOW_TIMES = per_version_max_show_times;
        COMMENT_INTERVAL_DAY = comment_interval_day;
    }
    public static String getShowTimesKey() {
        return VERSION_SHOW_TIMES + SZUtil.getVersionCode();
    }
    public static void setUsed() {
        xmlUtil.put(USED, true);
        xmlUtil.put(USED_DATE,System.currentTimeMillis());
    }
    public static long getUsedTime(){
        return xmlUtil.getLong(USED_DATE);
    }
    public static boolean isCanShowFavourableComment() {
        if (xmlUtil.getInt(getShowTimesKey()) < PER_VERSION_MAX_SHOW_TIMES && xmlUtil.getBoolean(USED)&&SZDateUtil.isSameDay(new Date(),new Date(getUsedTime()))) {
            long spaceTime = System.currentTimeMillis() - xmlUtil.getLong(CLICK_COMMENT_TIME);
            switch (xmlUtil.getInt(CLICK_COMMENT_STATUS)) {
                case YES:
                    return spaceTime >= ONE_DAY * COMMENT_INTERVAL_DAY;
                case NO:
                    return spaceTime >= ONE_DAY * COMMENT_INTERVAL_DAY;
            }
            return true;
        }
        return false;
    }
    public static void showFavourableComment(Context context){
        showFavourableComment(context,null,null,null,null);
    }
    public static void showFavourableComment(final Context context,String title,String content,String ok,String cancel) {
        if(isCanShowFavourableComment()) {
            xmlUtil.put(getShowTimesKey(), (xmlUtil.getInt(getShowTimesKey()) + 1));
            final AlertWidget alertWidget = new AlertWidget(context);
            alertWidget.setCanceledOnTouchOutside(false);
            alertWidget.setCancelable(false);
            alertWidget.setTitle(title!=null?title:"亲，赏个好评吧！");
            alertWidget.setContent(content!=null?content:"小的日夜努力写此应用，用过的爷赏个五星好评可好？");
            alertWidget.setOKListener(ok!=null?ok:"赏好评", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alertWidget.close();
                    xmlUtil.put(CLICK_COMMENT_STATUS, YES);
                    xmlUtil.put(CLICK_COMMENT_TIME, System.currentTimeMillis());
                    SZUtil.launchAppStoreDetail(context, SZApp.getInstance().getPackageName(), null);
                }
            });
            alertWidget.setCancelListener(cancel!=null?cancel:"不赏，我就看看", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alertWidget.close();
                    xmlUtil.put(CLICK_COMMENT_STATUS, NO);
                    xmlUtil.put(CLICK_COMMENT_TIME, System.currentTimeMillis());
                }
            });
            alertWidget.show();
        }
    }
}
