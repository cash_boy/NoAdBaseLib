package com.cn.shuangzi.util;

import android.os.Environment;


import com.cn.shuangzi.SZApp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;


public class SZFileUtil {
	public static final String IMG_CACHE_DISK = "imgCache";
	public static String FILE_APP = "/shuangzi/";
	private static SZFileUtil fileCache;
	private static String appPath;
	private File imgFile;
	private File tmpFile;
	private File downloadFile;
	private File cacheFile;

	private SZFileUtil() {
		if(!FILE_APP.contains(appPath)) {
			FILE_APP = FILE_APP + appPath;
		}
		// 如果有SD卡则在SD卡中建一个目录存放缓存的图片
		// 没有SD卡就放在系统的缓存目录中
		try {
			cacheFile = SZApp.getInstance().getExternalCacheDir();
			if (hasSDCard()) {
				imgFile = new File(Environment
						.getExternalStorageDirectory().getAbsolutePath()
						+ FILE_APP+"/"+IMG_CACHE_DISK+"/");
				tmpFile = new File(Environment
						.getExternalStorageDirectory().getAbsolutePath()
						+ FILE_APP+"/tmp/");
				downloadFile = new File(Environment
						.getExternalStorageDirectory().getAbsolutePath()
						+ FILE_APP+"/download/");
			} else {
				imgFile = new File(cacheFile,IMG_CACHE_DISK);
				tmpFile = new File(cacheFile,"tmp");
				downloadFile = new File(cacheFile,"download");
			}
			if (!cacheFile.exists()) {
				cacheFile.mkdirs();
			}
			if (!tmpFile.exists()) {
				tmpFile.mkdirs();
			}
			if (!downloadFile.exists()) {
				downloadFile.mkdirs();
			}
			if (!imgFile.exists()) {
				imgFile.mkdirs();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void init(String appPath){
		SZFileUtil.appPath = appPath;
	}
	public static SZFileUtil getInstance() {
		if(appPath == null){
			throw new IllegalArgumentException("appPath not init ...");
		}
		if (fileCache == null) {
			fileCache = new SZFileUtil();
		}
		return fileCache;
	}

	public String getTmpPathName(){
		return tmpFile.getAbsolutePath();
	}
	public File getOneTmpPic(){
		return new File(tmpFile,"tmp.jpg");
	}

	public void clearTmpFiles() {
		clearFile(tmpFile);
	}
	// 获得缓存目录
	public File getCacheFile() {
		return cacheFile;
	}
	// 获得图片缓存目录
	public File getImgCacheFile() {
		return imgFile;
	}
	public String getImgCacheFilePath() {
		return imgFile.getAbsolutePath();
	}
//	public String getSkinsPathName(){
//		return skinsFile.getAbsolutePath();
//	}

	// 获得下载路径
	public String getDownloadPathName() {
		if (downloadFile != null)
			return downloadFile.getAbsolutePath();
		return null;
	}

	// 是否有SD卡
	public boolean hasSDCard() {
		return Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);
	}

	// 按照文件夹获取该文件夹大小
	public String getCacheFileSizeByFolder(String folder) {
		try {
			return formatFileSize(getFileSize(new File(folder)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	// 获得缓存文件夹大小
	public String getCacheFileSize() {
		try {
			return formatFileSize(getFileSize(cacheFile));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	public String getAllCacheFileSize(){
		return formatFileSize(getFileSize(getCacheFile())+getFileSize(getImgCacheFile()));
	}
	public void clearAllCacheFile(){
		clearImgCacheFiles();
		clearCacheFiles();
	}
	/**
	 * 获得文件夹下的文件
	 * 
	 * 
	 * @param f
	 * @return
	 */
	public File[] getFilesByFolder(File f) {
		File flist[] = f.listFiles();
		return flist;
	}

	// 递归
	public long getFileSize(File f)// 取得文件夹大小
	{
		long size = 0;
		File fileList[] = f.listFiles();
		for (int i = 0; i < fileList.length; i++) {
			if (fileList[i].isDirectory()) {
				size = size + getFileSize(fileList[i]);
			} else {
				size = size + fileList[i].length();
			}
		}
		return size;
	}

	public String formatFileSize(long fileS) {// 转换文件大小
		if (fileS == 0)
			return "";
		DecimalFormat df = new DecimalFormat("#.00");
		String fileSizeString = "";
		if (fileS < 1024) {
			fileSizeString = df.format((double) fileS) + "B";
		} else if (fileS < 1048576) {
			fileSizeString = df.format((double) fileS / 1024) + "K";
		} else if (fileS < 1073741824) {
			fileSizeString = df.format((double) fileS / 1048576) + "M";
		} else {
			fileSizeString = df.format((double) fileS / 1073741824) + "G";
		}
		return fileSizeString;
	}

	/**
	 ********************
	 * 清空制定缓存文件夹
	 * @param folder
	 ********************
	 */
	public void clearCacheFiles(String folder) {
		try {
			File[] files = new File(cacheFile.getAbsolutePath() + "/" + folder)
					.listFiles();
			if (files == null)
				return;
			for (File f : files)
				f.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 ********************
	 * 清空图片缓存文件夹
	 ********************
	 */
	public void clearImgCacheFiles() {
		try {
			File[] files = new File(imgFile.getAbsolutePath())
					.listFiles();
			if (files == null)
				return;
			for (File f : files)
				f.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 ********************
	 * 清空临时图片缓存文件夹
	 ********************
	 */
	public void clearDownloadFiles() {
		try {
			File[] files = new File(downloadFile.getAbsolutePath())
					.listFiles();
			if (files == null)
				return;
			for (File f : files)
				f.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 ********************
	 *清空所有缓存文件夹
	 ********************
	 */
	public void clearCacheFiles() {
		clearFile(cacheFile);
	}
	public void clearFile(File file){
		File[] fileList = file.listFiles();
		if(fileList!=null) {
			for (int i = 0; i < fileList.length; i++) {
				if (fileList[i].isDirectory()) {
					clearFile(fileList[i]);
				} else {
					fileList[i].delete();
				}
			}
		}
	}
	/**
	 * 把文件拷贝到某一目录下,并删除源文件
	 * 
	 * @param srcFile
	 * @param destDir
	 * @return
	 */
	public boolean moveFileToDir(String srcFile, String destDir) {
		File fileDir = new File(destDir);// 目标文件
		File fileSrc = new File(srcFile);// 源文件
		if (!fileDir.exists()) {
			fileDir.mkdir();
		}
		String destFile = destDir + "/" + fileSrc.getName();
		try {
			InputStream streamFrom = new FileInputStream(srcFile);
			OutputStream streamTo = new FileOutputStream(destFile);
			byte buffer[] = new byte[1024];
			int len;
			while ((len = streamFrom.read(buffer)) > 0) {
				streamTo.write(buffer, 0, len);
			}
			streamFrom.close();
			streamTo.close();
			fileSrc.delete();
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}
}
