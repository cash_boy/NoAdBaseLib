package com.cn.shuangzi.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * 精确运算工具类
 *
 * @author cn
 */
public class SZArithUtil {
    private static final int DEF_DIV_SCALE = 2;


    public static double add(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.add(b2).doubleValue();
    }

    public static double sub(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.subtract(b2).doubleValue();
    }

    public static double mul(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.multiply(b2).doubleValue();
    }

    public static double div(double v1, double v2) {
        return div(v1, v2, DEF_DIV_SCALE);
    }

    public static double div(double v1, double v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static double round(double v, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b = new BigDecimal(Double.toString(v));
        BigDecimal one = new BigDecimal("1");
        return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static String getDoubleWithScale2(double value) {
        DecimalFormat df = new DecimalFormat(".##");
        return df.format(value);
    }

    public static String getDoubleWithScale1(double value) {
        DecimalFormat df = new DecimalFormat(".#");
        return df.format(value);
    }

    public static String subWithStringReturn(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        try {
            return String.valueOf(b1.subtract(b2).longValueExact());
        } catch (Exception e) {
        }
        return b1.subtract(b2).toString();
    }

    public static String toStringReturn(double d) {
        d = round(d, 2);
        BigDecimal bd = new BigDecimal(Double.toString(d));
        try {
            return String.valueOf(bd.longValueExact());
        } catch (Exception e) {
        }
        return bd.toString();
    }

    public static String addWithStringReturn(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        try {
            return String.valueOf(b1.add(b2).longValueExact());
        } catch (Exception e) {
        }
        return b1.add(b2).toString();
    }

}
